﻿using System;
using System.Collections.Generic;

namespace CollectionControlError
{
    class Program
    {
        static void Main(string[] args)
        {
            // Comment out the ones you dont want to render

            ShowcaseCollections();
            ShowcaseControlStructures();
            ShowcaseErrorHandling();
        }

        private static void ShowcaseCollections()
        {
            Console.WriteLine("Array section");
            /*
             * Demonstrate 1D arrays of various types and lengths
             *  -string
             *  -int
             * Show different ways of declaring and instantiating arrays
             */

            // CODE
            string[] myStringArray = new string[3];
            myStringArray[0] = "Array";
            myStringArray[1] = "of";
            myStringArray[2] = "words";
            //myStringArray[3] = "this will throw an error";

            int[] myIntArray = new int[] { 1, 4, 5, 12, 18 };
            // ---

            /*
             * Demonstrate 2D arrays [row, col]
             */

            // CODE
            int[,] my2DIntArray = new int[5, 5];
            my2DIntArray[0, 1] = 5;
            my2DIntArray[3, 4] = 7;
            //my2DIntArray[5, 5] = 8; // Will throw an error

            int[,] another2DIntArray = new int[,] { { 1, 2 }, { 3, 4 }, { 5, 6 }, { 7, 8 } };

            Console.WriteLine(another2DIntArray[0,0]); // Will print 1
            Console.WriteLine(another2DIntArray[2, 1]); // Will print 6
            // ---

            /*
             * Demonstrate commonly used collections
             *  -List
             *  -Queue
             *  -Stack
             *  -Dictionary
             * Will demonstrate simple functionality, as the more complicated ones 
             * require knowledge of concepts not yet covered.
             */

            // List - declare, add, addrange
            // CODE
            List<string> myStringList = new List<string>();
            myStringList.Add("Boom");
            myStringList.Add("Clap");
            List<string> otherStringList = new List<string>();
            otherStringList.Add("Hmm");
            otherStringList.AddRange(myStringList); // Will contain: Hmm, Boom, Clap
            // ---

            // Queue - declare, enqueue, peek, dequeue
            Console.WriteLine("\nQueue section");
            // CODE
            Queue<int> myIntQueue = new Queue<int>();
            myIntQueue.Enqueue(22);
            myIntQueue.Enqueue(35);
            myIntQueue.Enqueue(9);
            myIntQueue.Enqueue(13);
            Console.WriteLine(myIntQueue.Peek()); // Will show the first one in the que, 22.
            int myDequedItem = myIntQueue.Dequeue();
            Console.WriteLine(myDequedItem); // Will be the first item in the que, 22.
            Console.WriteLine(myIntQueue.Peek()); // Now 35 is in the front.
            // ---

            // Stack - declare, push, peek, pop
            Console.WriteLine("\nStack section");
            // CODE
            Stack<int> myIntStack = new Stack<int>();
            myIntStack.Push(99);
            myIntStack.Push(13);
            myIntStack.Push(35);
            myIntStack.Push(2);
            myIntStack.Push(7);
            Console.WriteLine(myIntStack.Peek()); // Will show the item at the top in the stack, 7.
            int myPoppedItem = myIntStack.Pop();
            Console.WriteLine(myPoppedItem);
            Console.WriteLine(myIntStack.Peek()); // Now 2 is at the top of the stack.
            // ---

            // Dictionary - declare, contains, remove
            Console.WriteLine("\nDictionary section");
            // CODE
            Dictionary<int, string> myDictionary = new Dictionary<int, string>();
            myDictionary.Add(1, "Monday");
            myDictionary.Add(2, "Tuesday");
            myDictionary.Add(3, "Wednesday");
            myDictionary.Add(4, "Thursday");
            myDictionary.Add(5, "Friday");
            Console.WriteLine(myDictionary.ContainsValue("Wednesday"));
            Console.WriteLine(myDictionary.ContainsValue("Saturday"));
            myDictionary.Remove(3);
            Console.WriteLine(myDictionary.ContainsValue("Wednesday")); // Will show false as Wednesday was removed
            // ---
        }

        private static void ShowcaseControlStructures()
        {
            /*
             * SELECTION
             *  -if
             *  -if-else
             *  -switch
             */

            Console.WriteLine("\nIf statements");
            bool myBool = true;
            string myWord = "Alpaca";
            int myNumber = 5;

            // Normal IF
            Console.WriteLine("\nIf");
            // CODE
            if (myWord == "Alpaca")
            {
                Console.WriteLine("myWord is equal to Alpaca");
            }

            if(myNumber == 10)
            {
                // This will be skipped
                Console.WriteLine("myNumber is 10");
            }

            if(myBool)
            {
                // Notice no ==, the variable is a boolean already
                Console.WriteLine("Big true");
            }
            // ---

            // IF-ELSE
            Console.WriteLine("\nIf-else");
            // CODE
            if (myNumber > 10)
            {
                Console.WriteLine("My number is larger than 10");
            } else
            {
                Console.WriteLine("My number is smaller than 10");
            }

            if(myNumber > 10)
            {
                Console.WriteLine("My number is larger than 10");
            } else if(myNumber > 3)
            {
                Console.WriteLine("My number is larger than 3, but not larger than 10");
            }

            if(myNumber < 10 && myNumber > 5)
            {
                Console.WriteLine("My number is between 5 and 10, excluding 5 and 10");
            } else
            {
                Console.WriteLine("My number is outside the range 5 to 10, excluding 5 and 10");
            }
            // ---

            // Switch
            Console.WriteLine("\nSwitch statements");
            int numLlama = 5;
            // CODE

            switch (numLlama){
                case 0:
                    Console.WriteLine("There are no llamas :(");
                    break;
                case 3:
                    Console.WriteLine("LLama party!");
                    break;
                case 5:
                    Console.WriteLine("May want to open a window, there are quite a few llamas");
                    break;
                case 10:
                    Console.WriteLine("The police have been called, this is flat out irresponsible.");
                    break;
                default:
                    Console.WriteLine("I dont know what to do with the amount of llamas you have, we can only cater for exact values.");
                    break;
            }
            // ---

            /*
             * REPETITION
             *  -for
             *  -foreach
             *  -while
             *  -do-while
             */

            // For loop - care for off by one errors
            string[] llamas = new string[] { "John", "Clive", "Reginald", "George", "Thamos", "Esteban" };

            Console.WriteLine("\nFor, ftl"); // First to last
            // CODE
            for (int i = 0; i < llamas.Length; i++)
            {
                Console.WriteLine(llamas[i]);
            }
            // ---
            Console.WriteLine("\nFor ltf"); // Last to first
            // CODE
            for (int i = llamas.Length-1; i >= 0; i--)
            {
                Console.WriteLine(llamas[i]);
            }
            // ---

            // Foreach
            Console.WriteLine("\nForeach");
            // CODE
            foreach (string llama in llamas)
            {
                Console.WriteLine(llama);
            }
            // ---

            // While
            Console.WriteLine("\nWhile");
            // CODE
            while (numLlama < 10)
            {
                Console.WriteLine("Acceptable amount of llamas {0}", numLlama);
                numLlama++;
            }
            // ---

            // Do-while
            Console.WriteLine("\nDo-while");
            // CODE
            do
            {
                Console.WriteLine("{0} llamas in the party.", numLlama);
                numLlama++;
            } while (numLlama < 5);
            // ---
        }

        private static void ShowcaseErrorHandling()
        {
            /*
             * Demonstrate various try-catch variants
             *  -try-catch
             *  -try-multiple catch
             *  -try-catch-finally
             */

            // Try-catch
            Console.WriteLine("\nTry-catch");
            // CODE
            try
            {
                int myVar = Convert.ToInt32("1e3");
            } catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            // ---

            // Try-multicatch
            Console.WriteLine("\nTry-catch multi");
            // CODE
            try
            {
                int myVar = Convert.ToInt32("26e"); // Change this to 26 to trigger indexoutofrangeexception
                int[] intArray = new int[] { 1, 2, 3, 4 };
                for (int i = 0; i <= intArray.Length; i++)
                {
                    intArray[i] = 1;
                }
            } catch(IndexOutOfRangeException ex)
            {
                Console.WriteLine("Index out of range: " + ex.Message);
            } catch(Exception ex)
            {
                Console.WriteLine("General exception: " + ex.Message);
            }
            // ---

            // Try-catch-finally
            Console.WriteLine("\nTry-catch-finally");
            // CODE
            try
            {
                int myVar = Convert.ToInt32("26e");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("Dodgey code done");
            }
            // ---
        }
    }
}
